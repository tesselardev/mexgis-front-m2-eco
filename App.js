import React, { Component } from 'react';
import './App.css';
import Navigation from './components/Navigation/Navigation';
import Logo from './components/Logo/Logo';
import Signin from './components/Signin/Signin';
import Login from './components/Login/Login'
import Tablehome from './components/Tablehome/Tablehome'
import { products } from './components/Tablehome/Products'; 


let mexgisUrl = 'http://ec2-35-161-29-189.us-west-2.compute.amazonaws.com:3000';

class App extends Component {
  constructor(){
    super();
    this.state = {
      route: 'login',
      isLoggedIn: false,
    }
    let mexgisUrl = 'http://ec2-35-161-29-189.us-west-2.compute.amazonaws.com:3000';
  }
  onRouteChange = (route) =>{
      if(route === 'login'){
        this.setState({isLoggedIn: false})
      }else if (route === 'table' || route === 'signin'){
        this.setState({isLoggedIn: true});
      }
      this.setState({route: route})
      console.log('Route:',route);
      console.log('isLoggedIn:',this.state.isLoggedIn);
  }
  render() {
    return (
      <div className="App">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossOrigin="anonymous"/>
      <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans" rel="stylesheet"/>
        <header className="fl w-100">
          <Logo />
          <Navigation onRouteChange={this.onRouteChange} route={this.state.route}/>
        </header>
        <section className="container-t fl w-100">
          { this.state.route === 'table' ?
          <Tablehome />
          : (
            this.state.route === 'login' ?
            <Login onRouteChange={this.onRouteChange} />
            : <Signin onRouteChange={this.onRouteChange} />
            )
          }
        </section>
        <footer className="fl w-100 copyright">
          <div className="pa3">Copyright © 2018 MexGis Todos los derechos reservados.</div>
        </footer>
      </div>
    );
  }
}

export default App;
